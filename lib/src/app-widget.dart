import 'package:contact_me/src/profile/profile-bloc.dart';
import 'package:contact_me/src/index/index-bloc.dart';
import 'package:contact_me/src/index/index-widget.dart';
import 'package:contact_me/src/shared/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_admob/firebase_admob.dart';

class AppWidget extends StatelessWidget {


  static MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    keywords: <String>['flutterio', 'beautiful apps'],
    contentUrl: 'https://flutter.io',
    childDirected: false,// or MobileAdGender.female, MobileAdGender.unknown
    testDevices: <String>[], // Android emulators are considered test devices
  );

  BannerAd myBanner = BannerAd(
    // Replace the testAdUnitId with an ad unit id from the AdMob dash.
    // https://developers.google.com/admob/android/test-ads
    // https://developers.google.com/admob/ios/test-ads

    //adUnitId: BannerAd.testAdUnitId,
    adUnitId: "ca-app-pub-4187482411999934/1542631907",
    size: AdSize.smartBanner,
    targetingInfo: targetingInfo,
    listener: (MobileAdEvent event) {
      print("BannerAd event is $event");
    },
  );

  @override
  Widget build(BuildContext context) {

    myBanner..load()..show(anchorType: AnchorType.bottom);
    FirebaseAdMob.instance.initialize(appId: "ca-app-pub-4187482411999934/1542631907");
    //FirebaseAdMob.instance.initialize(appId: BannerAd.testAdUnitId);
    indexBloc.carregaLista();
    profileBloc.carregaProfile();

    return MaterialApp(
        routes: routes,
        debugShowCheckedModeBanner: false,
        home: IndexPage()
    );
  }
}