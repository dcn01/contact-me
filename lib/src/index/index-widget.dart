import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:contact_me/src/index/index-bloc.dart';
import 'package:contact_me/src/index/index-model.dart';
import 'package:contact_me/src/profile/profile-bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:barcode_scan/barcode_scan.dart';

class IndexPage extends StatefulWidget {
  IndexPage({Key key}) : super(key: key);

  @override
  _IndexPageState createState() => new _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {

  MaskedTextController _maskedTextController;
  TextEditingController _textControllerEmail;
  TextEditingController _textControllerNome;

  final formPessoaKey = GlobalKey<FormState>();
  final scaffoldPessoaKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // sempre q tu quiser que uma coisa só aconteça uma vez , tu usa o método
    // initState , ele é um método que roda antes do widget aparecer na tela
    // ele vai instanciar o seu controller aqui
    // e vai destruir ele no método dispose , então initState - construção e dispose - destruição
    // é mt importante usar esses caras
    _textControllerEmail = TextEditingController();
    _textControllerNome = TextEditingController();
    _maskedTextController =  MaskedTextController(
      mask: "(00)00000-0000",
    )..addListener(_textMaskListener);

    super.initState();
  }

  void _textMaskListener(){
    _maskedTextController.updateText(_maskedTextController.value.text);
  }

  @override
  void dispose(){
    // como esse cara é um controller , ele consome recursos , então sempre
    // que tu tiver controllers , feche eles no final , blz ?
    // use o dispose de uma stateful widget pra mandar todos os controllers
    // pra pqp euheuehuehe , assim vc libera recursos
    _maskedTextController.dispose();
    _textControllerEmail.dispose();
    _textControllerNome.dispose();
    super.dispose();
  }

  _salvaPessoa(IndexBloc bloc) {
    final form = formPessoaKey.currentState;
    if (form.validate()) {
      form.save();
      bloc.add();
      bloc.reset();
      _showSnackBar("Contato salvo.");
    }
  }

  _editarPessoa(IndexBloc bloc, Pessoa pessoa) {
    bloc.salvaNome(pessoa.nome);
    bloc.salvaTelefone(pessoa.telefone);
    bloc.salvaEmail(pessoa.email);
    bloc.salvaPessoa(pessoa);
  }

  _removePessoa(IndexBloc bloc, Pessoa pessoa) {
    bloc.salvaNome(pessoa.nome);
    bloc.salvaTelefone(pessoa.telefone);
    bloc.salvaEmail(pessoa.email);
    bloc.salvaPessoa(pessoa);
    bloc.remove();
    bloc.reset();
  }

  _ligar(IndexBloc bloc, Pessoa pessoa) {
    bloc.salvaTelefone(pessoa.telefone);
    bloc.ligar();
  }

  _menssagem(IndexBloc bloc, Pessoa pessoa) {
    bloc.salvaTelefone(pessoa.telefone);
    bloc.menssagem();
  }

  _email(IndexBloc bloc, Pessoa pessoa) {
    bloc.salvaEmail(pessoa.email);
    bloc.email();
  }

  _whatsApp(IndexBloc bloc, Pessoa pessoa) {
    bloc.salvaTelefone(pessoa.telefone);
    bloc.whatsApp();
  }

  Widget _listaPessoas(IndexBloc bloc) {
    return StreamBuilder(
      stream: bloc.outList,
      builder: (BuildContext context, AsyncSnapshot<List<Pessoa>> snapshot) {
        if (snapshot.hasData) {
          return Container(
            child: ListView.builder(
              itemBuilder: (context, index) {
                if (index < snapshot.data.length) {
                  return ClipPath(
                    child: Slidable(
                      child: Column(
                        children: <Widget>[
                          GestureDetector(
                            onLongPress: (){
                              _removePessoa(
                                  bloc,
                                  snapshot.data.elementAt(index)
                              );
                            },
                            onTap: () {
                              Navigator.pop(context);
                              _editarPessoa(
                                  bloc,
                                  snapshot.data.elementAt(index)
                              );
                            },
                            child: Card(
                              color: Color.fromRGBO(147, 251, 255, 0.7),
                              elevation: 4,
                              child: ListTile(
                                title: Text(snapshot.data
                                    .elementAt(index)
                                    .nome),
                                subtitle: Text(snapshot.data
                                    .elementAt(index)
                                    .telefone),
                              ),
                            ),
                          ),
                        ],
                      ),
                      delegate: SlidableStrechDelegate(),
                      actionExtentRatio: 0.25,
                      actions: <Widget>[
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)
                          ),
                          elevation: 2,
                          color: Colors.orange,
                          child: IconSlideAction(
                            caption: 'Mensagem',
                            color: Colors.transparent,
                            icon: Icons.message,
                            onTap: () => _menssagem(bloc, snapshot.data.elementAt(index))
                          ),
                        ),
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)
                          ),
                          elevation: 2,
                          color: Colors.green,
                          child: IconSlideAction(
                            caption: 'Ligar',
                            color: Colors.transparent,
                            icon: Icons.phone,
                            onTap: () => _ligar(bloc, snapshot.data.elementAt(index))
                          ),
                        ),
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)
                          ),
                          elevation: 2,
                          color: Colors.blue,
                          child: IconSlideAction(
                            caption: 'E-mail',
                            color: Colors.transparent,
                            icon: Icons.email,
                            onTap: () => _email(bloc, snapshot.data.elementAt(index))
                          ),
                        ),
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)
                          ),
                          color: Colors.lightGreen,
                          child: IconSlideAction(
                            caption: 'WhatsApp',
                            color: Colors.transparent,
                            icon: MdiIcons.whatsapp ,
                            onTap: () => _whatsApp(bloc, snapshot.data.elementAt(index))
                          ),
                        ),
                      ],
                    ),
                  );
                }
              }
            ),
          );
        } else {
          return Divider();
        }
      }
    );
  }

  Widget _cadastroPessoa(IndexBloc bloc) {
    return Form(
      key: formPessoaKey,
      child: SafeArea(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8),
              child: Chip(
                avatar: Icon(Icons.person),
                label: Text(
                  "Contato",
                  textScaleFactor: 2.0,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0)
                ),
                color: Color.fromRGBO(37, 247, 255, 0.5),
                elevation: 6,
                child: SafeArea(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: StreamBuilder(
                          stream: bloc.outNome,
                          initialData: bloc.nomeValue,
                          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                            _textControllerNome.text = snapshot.data;
                            return TextFormField(
                              validator: (value){
                                if(value.isEmpty){
                                  return "Informe o nome do contato.";
                                }
                              },
                              controller: _textControllerNome,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                icon: Icon(Icons.perm_contact_calendar),
                                labelText: "Nome",
                              ),
                              onSaved: (String val) {
                                bloc.salvaNome(val);
                              }
                            );
                          }
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: StreamBuilder(
                          initialData: bloc.telefoneValue,
                          stream: bloc.outTelefone,
                          builder: (BuildContext context, AsyncSnapshot<String> snapshot){
                            _maskedTextController.updateText(snapshot.data);
                            return TextFormField(
                              validator: (value){
                                if(value.isEmpty){
                                  return "Informe o telefone do contato.";
                                }
                              },
                              keyboardType: TextInputType.phone,
                              controller: _maskedTextController,
                              decoration: InputDecoration(
                                icon: Icon(Icons.phone),
                                labelText: "Telefone",
                                hintText: "(00)00000-0000",
                              ),
                              onSaved: (String val) {
                                bloc.salvaTelefone(val);
                              }
                            );
                          }
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: StreamBuilder(
                          initialData: bloc.emailValue,
                          stream: bloc.outEmail,
                          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                            _textControllerEmail.text = snapshot.data;
                            return TextFormField(
                              validator: (value){
                                if(value.isEmpty){
                                  return "Informe o email do contato.";
                                }
                              },
                              keyboardType: TextInputType.emailAddress,
                              controller: _textControllerEmail,
                              decoration: InputDecoration(
                                icon: Icon(Icons.email),
                                labelText: "Email",
                              ),
                              onSaved: (String val) {
                                bloc.salvaEmail(val);
                              }
                            );
                          }
                        ),
                      ),
                      Divider(
                        color: Colors.transparent,
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        )
      ),
    );
  }

  _showSnackBar(String text){
    scaffoldPessoaKey.currentState.showSnackBar(SnackBar(content: Text(text)));
  }

  Future _scanQR() async {
    String qrResult = await BarcodeScanner.scan();
    print(qrResult);

    List<String> campos = qrResult.split("||");

    if(campos != null && campos.isNotEmpty){
      indexBloc.salvaNome(campos[0]);
      indexBloc.salvaTelefone(campos[1]);
      indexBloc.salvaEmail(campos[2]);

      indexBloc.add();
      indexBloc.reset();

      _showSnackBar("Contato ${campos[0]} salvo.");
    }else if(campos.length != 3){
      _showSnackBar("Qr Code inválido!");
    }else{
      _showSnackBar("Qr Code inexistente!");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0,0,0,50),
      child: Scaffold(
        key: scaffoldPessoaKey,
        appBar: AppBar(
          title: Text("Contact Me"),
          actions: <Widget>[
            IconButton(
              tooltip: "Profile",
              icon: Icon(
                MdiIcons.face,
              ),
              onPressed: (){Navigator.of(context).pushNamed('/profile');}
            )
          ],
        ),
        drawer: SafeArea(
          child: Drawer(
            child: Column(
              children: <Widget>[
                BlocProvider<ProfileBloc>(
                  bloc: ProfileBloc(),
                  child: UserAccountsDrawerHeader(
                    currentAccountPicture: Icon(MdiIcons.face, color: Colors.white70, size: 70),
                    otherAccountsPictures: <Widget>[
                      GestureDetector(
                        onTap: (){Navigator.of(context).pushNamed('/qrCode');},
                        child: Icon(MdiIcons.qrcode, color: Colors.black54, size: 50),
                      )
                    ],
                    accountName: Column(
                      children: <Widget>[
                        StreamBuilder(
                          stream: profileBloc.outNome,
                          builder: (BuildContext context, AsyncSnapshot snapshot) {
                            return Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Nome: " + (snapshot.hasData ? snapshot.data.toString() : ""),
                                style: TextStyle(color: Colors.black87),
                              ),
                            );
                          },
                        ),
                        StreamBuilder(
                          stream: profileBloc.outTelefone,
                          builder: (BuildContext context, AsyncSnapshot snapshot) {
                            return Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Telefone: " + (snapshot.hasData ? snapshot.data.toString() : ""),
                                style: TextStyle(color: Colors.black87),
                              ),
                            );
                          },
                        ),
                        StreamBuilder(
                          stream: profileBloc.outEmail,
                          builder: (BuildContext context, AsyncSnapshot snapshot) {
                            return Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "E-mail: " + (snapshot.hasData ? snapshot.data.toString() : ""),
                                style: TextStyle(color: Colors.black87),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                    accountEmail: null,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Chip(
                    avatar: Icon(Icons.people),
                    label: Text(
                      "Contatos",
                      textScaleFactor: 2.0,
                    ),
                  ),
                ),
                Expanded(child: _listaPessoas(indexBloc))
              ],
            )
          ),
        ),
        body: ListView(
          scrollDirection: Axis.vertical,
          children: <Widget>[
            _cadastroPessoa(indexBloc),
            ButtonBar(
              alignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  shape:  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)
                  ),
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0,0,8,0),
                        child: Icon(Icons.add, color: Colors.white,),
                      ),
                      Text(
                          "Salvar Contato",
                          style: TextStyle(color: Colors.white)
                      )
                    ],
                  ),
                  onPressed: (){_salvaPessoa(indexBloc);},
                  color: Colors.blueAccent,
                  highlightColor: Colors.blue,
                ),
                RaisedButton(
                  shape:  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)
                  ),
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0,0,8,0),
                        child: Icon(MdiIcons.qrcodeScan, color: Colors.white,),
                      ),
                      Text(
                          "Ler Qr Code",
                          style: TextStyle(color: Colors.white)
                      )
                    ],
                  ),
                  onPressed: (){_scanQR();},
                  color: Colors.blueAccent,
                  highlightColor: Colors.blue,
                )
              ],
            )
          ]
        ),
      ),
    );
  }
}