import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:contact_me/src/profile/profile-model.dart';
import 'package:contact_me/src/shared/service/database.dart';
import 'package:rxdart/rxdart.dart';

class ProfileBloc extends BlocBase{

  DatabaseHelper databaseHelper = DatabaseHelper.internal();

  var _nome = BehaviorSubject<String>(seedValue: "");

  Stream<String> get outNome => _nome.stream;

  Sink<String> get inNome => _nome.sink;

  String get nomeValue => _nome.value;

  void salvaNome(String nome){
    inNome.add(nome);
    print("nome");
  }

  var _telefone = BehaviorSubject<String>(seedValue: "");

  Stream<String> get outTelefone => _telefone.stream;

  Sink<String> get inTelefone => _telefone.sink;

  String get telefoneValue => _telefone.value;

  void salvaTelefone(String telefone){
    inTelefone.add(telefone);
    print("telefone");
  }

  var _email = BehaviorSubject<String>(seedValue: "");

  Stream<String> get outEmail => _email.stream;

  Sink<String> get inEmail => _email.sink;

// getter do valor mais recente emitido pelo subject
  String get emailValue => _email.value;

  void salvaEmail(String email){
    inEmail.add(email);
    print("email");
  }

  void carregaProfile() async{
    Profile profile = await databaseHelper.getProfile();
    if(profile != null) {
      inNome.add(profile.nome);
      inTelefone.add(profile.telefone);
      inEmail.add(profile.email);
      print(profile.toString());
    }
  }

  void add(){
    Profile value = Profile();
    value.nome = _nome.value;
    value.telefone = _telefone.value;
    value.email = _email.value;

    if(databaseHelper.getProfile() != null) {
      databaseHelper.deletProfile();
    }

    databaseHelper.insertProfile(value);

    print("salva profile");
  }

  @override
  void dispose() {
    _nome.close();
    _telefone.close();
    _email.close();
  }

}

ProfileBloc profileBloc = ProfileBloc();