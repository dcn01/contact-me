class Profile{

  String _nome;
  String _telefone;
  String _email;


  Profile();

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  String get telefone => _telefone;

  set telefone(String value) {
    _telefone = value;
  }

  String get nome => _nome;

  set nome(String value) {
    _nome = value;
  }

  @override
  String toString() {
    return 'Profile{_nome: $_nome, _telefone: $_telefone, _email: $_email}';
  }

  @override
  bool operator ==(other) {
    Profile p2 = other;
    if(this.email == p2.email && this.telefone == p2.telefone && this.nome == p2.nome)
      return true;
    else
      return false;
  }

  @override
  int get hashCode => super.hashCode;

  Profile.fromMap(Map<String, dynamic> map) {
    _nome = map['nome'];
    _telefone = map['telefone'];
    _email = map['email'];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'nome': this._nome,
      'telefone': this._telefone,
      'email': this._email
    };
    return map;
  }

}