import 'package:contact_me/src/profile/profile-bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => new _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>{

  MaskedTextController _maskedTextController;
  bool initVal = true;
  final formProfileKey = GlobalKey<FormState>();
  final scaffoldProfileKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // sempre q tu quiser que uma coisa só aconteça uma vez , tu usa o método
    // initState , ele é um método que roda antes do widget aparecer na tela
    // ele vai instanciar o seu controller aqui
    // e vai destruir ele no método dispose , então initState - construção e dispose - destruição
    // é mt importante usar esses caras
    _maskedTextController =  MaskedTextController(
      mask: "(00)00000-0000",
    )..addListener(_textMaskListener);

    super.initState();
  }

  void _textMaskListener(){
    if(_maskedTextController.value.text.length > 1){
      _maskedTextController.updateText(_maskedTextController.value.text);
    }
  }

  @override
  void dispose(){
    // como esse cara é um controller , ele consome recursos , então sempre
    // que tu tiver controllers , feche eles no final , blz ?
    // use o dispose de uma stateful widget pra mandar todos os controllers
    // pra pqp euheuehuehe , assim vc libera recursos
    _maskedTextController.dispose();
    super.dispose();
  }

  _confirmaPerfil(ProfileBloc bloc) {
    final form = formProfileKey.currentState;
    if (form.validate()) {
      form.save();
      bloc.add();
      _showSnackBar("Profile salvo.");
    }
  }


  Widget _cadastroProfile(ProfileBloc bloc){

    return Form(
      key: formProfileKey,
      child: SafeArea(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8),
                child: Chip(
                  avatar: Icon(MdiIcons.face),
                  label: Text(
                    "Profile",
                    textScaleFactor: 2.0,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)
                  ),
                  color: Color.fromRGBO(37, 247, 255, 0.5),
                  elevation: 6,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: StreamBuilder(
                            stream: bloc.outNome,
                            initialData: bloc.nomeValue,
                            builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                              return TextFormField(
                                  validator: (value){
                                    if(value.isEmpty){
                                      return "Informe o seu nome.";
                                    }
                                  },
                                  initialValue: snapshot.data,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    icon: Icon(Icons.perm_contact_calendar),
                                    labelText: "Nome",
                                  ),
                                  onSaved: (String val) {
                                    bloc.salvaNome(val);
                                  }
                              );
                            }
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: StreamBuilder(
                            initialData: bloc.telefoneValue,
                            stream: bloc.outTelefone,
                            builder: (BuildContext context, AsyncSnapshot<String> snapshot){
                              if(initVal) {
                                _maskedTextController.updateText(snapshot.data);
                                initVal = false;
                              }
                              return TextFormField(
                                  validator: (value){
                                    if(value.isEmpty){
                                      return "Informe o seu telefone.";
                                    }
                                  },
                                  keyboardType: TextInputType.phone,
                                  controller: _maskedTextController,
                                  decoration: InputDecoration(
                                    icon: Icon(Icons.phone),
                                    labelText: "Telefone",
                                    hintText: "(00)00000-0000",
                                  ),
                                  onSaved: (String val) {
                                    bloc.salvaTelefone(val);
                                  }
                              );
                            }
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: StreamBuilder(
                            initialData: bloc.emailValue,
                            stream: bloc.outEmail,
                            builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                              return TextFormField(
                                  validator: (value){
                                    if(value.isEmpty){
                                      return "Informe o seu email.";
                                    }
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                  initialValue: snapshot.data,
                                  decoration: InputDecoration(
                                    icon: Icon(Icons.email),
                                    labelText: "Email",
                                  ),
                                  onSaved: (String val) {
                                    bloc.salvaEmail(val);
                                  }
                              );
                            }
                        ),
                      ),
                      Divider(
                        color: Colors.transparent,
                      )
                    ],
                  ),
                ),
              )
            ],
          )
      ),
    );
  }

  _showSnackBar(String text){
    scaffoldProfileKey.currentState.showSnackBar(SnackBar(content: Text(text)));
  }

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.fromLTRB(0,0,0,50),
      child: Scaffold(
          key: scaffoldProfileKey,
          appBar: AppBar(
            title: Text("Profile"),
          ),
          body: ListView(
            children: <Widget>[
              _cadastroProfile(profileBloc)
            ],
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.check),
            onPressed: () {
              _confirmaPerfil(profileBloc);
            },
          )
      ),
    );
  }
}