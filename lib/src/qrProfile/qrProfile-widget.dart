import 'package:contact_me/src/profile/profile-bloc.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrProfilePage extends StatefulWidget {
  QrProfilePage({Key key}) : super(key: key);

  @override
  _QrProfilePageState createState() => new _QrProfilePageState();
}

class _QrProfilePageState extends State<QrProfilePage>{

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.fromLTRB(0,0,0,50),
      child: Scaffold(
          appBar: AppBar(
            title: Text("Qr Profile"),
          ),
          body: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8),
                child: Chip(
                  avatar: Icon(MdiIcons.qrcode),
                  label: Text(
                    "QR Code",
                    textScaleFactor: 2.0,
                  ),
                ),
              ),
              Center(
                child: Card(
                  margin: EdgeInsets.all(8),
                  elevation: 3,
                  child: QrImage(
                    data: profileBloc.nomeValue + "||" + profileBloc.telefoneValue + "||" + profileBloc.emailValue,
                    padding: EdgeInsets.all(8),
                    gapless: true,
                  ),
                ),
              ),
            ],
          )
      ),
    );
  }
}