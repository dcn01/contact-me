import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:contact_me/src/profile/profile-bloc.dart';
import 'package:contact_me/src/profile/profile-widget.dart';
import 'package:contact_me/src/qrProfile/qrProfile-widget.dart';
import 'package:flutter/material.dart';

final routes = {
  '/profile': (BuildContext context) => ProfilePage(),
  '/qrCode': (BuildContext context) => QrProfilePage()
};