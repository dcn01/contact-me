import 'package:contact_me/src/index/index-model.dart';
import 'package:contact_me/src/profile/profile-model.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if(_db != null){
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "main.db");
    var theDb = await openDatabase(path, version: 2, onCreate: _onCreate, onUpgrade: _onUpgrade, onDowngrade: _onDowngrade);
    return theDb;
  }

  void _onDowngrade(Database db, int oldVersion, int newVersion) {
    db.execute("DROP TABLE IF EXISTS Profile");
    db.execute("DROP TABLE IF EXISTS Pessoa");
    _onCreate(db, newVersion);
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) {
    db.execute("DROP TABLE IF EXISTS Profile");
    db.execute("DROP TABLE IF EXISTS Pessoa");
    _onCreate(db, newVersion);
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE Profile( nome TEXT"
            ", telefone TEXT"
            ", email TEXT)");
    await db.execute(
        "CREATE TABLE Pessoa(  id INTEGER PRIMARY KEY AUTOINCREMENT"
            ", nome TEXT"
            ", telefone TEXT"
            ", email TEXT)");
  }

  Future closeDb() async {
    var dbClient = await db;
    dbClient.close();
  }

  //Profile>>>>>>>>
  Future<Profile> insertProfile(Profile profile) async {
    var dbClient = await db;
    await dbClient.insert("Profile", profile.toMap());
    return profile;
  }

  Future<int> deletProfile() async {
    var dbClient = await db;
    return await dbClient.delete("Profile");
  }

  Future<int> updateProfile(Profile profile) async {
    var dbClient = await db;
    return await dbClient.update("Profile", profile.toMap());
  }

  Future<Profile> getProfile() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Profile",
        columns: ["nome", "telefone", "email"]);
    if (maps.length > 0) {
      return new Profile.fromMap(maps.first);
    }
    return null;
  }
  //<<<<<<<Profile
  //Pessoa>>>>>>>>
  Future<Pessoa> insertPessoa(Pessoa pessoa) async {
    var dbClient = await db;
    pessoa.id = await dbClient.insert("Pessoa", pessoa.toMap());
    return pessoa;
  }

  Future<int> deletPessoa(int id) async {
    var dbClient = await db;
    return await dbClient.delete("Pessoa", where: "id = ?", whereArgs: [id]);
  }

  Future<int> updatePessoa(Pessoa pessoa) async {
    var dbClient = await db;
    return await dbClient.update("Pessoa", pessoa.toMap(),
        where: "id = ?", whereArgs: [pessoa.id]);
  }

  Future<List<Pessoa>> getPessoas() async {
    List<Pessoa> pessoas = new List<Pessoa>();
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Pessoa");
    if (maps.length > 0) {
      for (Map pessoa in maps) {
        pessoas.add(Pessoa.fromMap(pessoa));
      }
    }
    return  pessoas;
  }

//<<<<<<<Pessoa
}